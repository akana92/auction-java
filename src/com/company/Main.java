package com.company;

import com.company.jsonUtils.JsonReaderAuction;
import com.company.jsonUtils.JsonWriterAuction;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String path = "src/products.json";
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonReaderAuction jsonReaderAuction = new JsonReaderAuction(path, gson);
        try {
            Auction[] auctions = jsonReaderAuction.getGoods();
            for (Auction auction : auctions){
                System.out.println(auction);
            }

            JsonWriterAuction writer = new JsonWriterAuction(path, gson, sc);
            for (int i = 1; i < auctions.length;) {
                while (true) {
                    System.out.print("Выберите товар из перечисленных: ");
                    i = sc.nextInt() - 1;
                        System.out.println(auctions[i]);
                        System.out.println("1. Выставить на аукцион\n2. Поднять цену\n3. Выдать победителю\n4. Снять с торгов\n5. Отобразить информацию о товаре\n6. Вернуться в список товаров");
                        System.out.print("Что вы хотели бы сделать?: ");
                        int b = sc.nextInt();
                        switch (b) {
                            case 1:
                                auctions[i].startSale();
                                writer.writeGoods(auctions);
                                return;
                            case 2:
                                auctions[i].raisePrice();
                                writer.writeGoods(auctions);
                                return;
                            case 3:
                                auctions[i].giveToTheWinner();
                                writer.writeGoods(auctions);
                                return;
                            case 4:
                                auctions[i].withdraw();
                                writer.writeGoods(auctions);
                                return;
                            case 5:
                                System.out.println(auctions[i]);
                                return;
                            case 6:
                                for (Auction auction : auctions) {
                                    System.out.println(auction);
                                }
                                break;
                    }
                }
            }
        }catch (IOException e){
            System.out.println("Вы ввели неверный товар");
        }
    }
}

