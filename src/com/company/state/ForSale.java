package com.company.state;

import com.company.Auction;
import com.company.CodeGenerator;

public class ForSale extends State{
    CodeGenerator code = new CodeGenerator();
    @Override
    public void startSale(Auction auction) {
        System.out.println("Товар уже участвует в торгах");
    }

    @Override
    public void raisePrice(Auction auction) {
        auction.setPrice(auction.getPrice()+100);
        System.out.println("Цена успешно повышена");
    }
    
    

    @Override
    public void withdraw(Auction auction) {
        if (auction.getPrice() == 200){
            auction.setState(new InStock());
            auction.setStateName("stock");
            System.out.println("Товар перенесен на склад");
        }else{
            System.out.println("Товар в резерве, можно только выдать");
        }
    }

    @Override
    public void giveToTheWinner(Auction auction) {
        if (auction.getPrice() == 0){
            System.out.println("Нельзя отдать товар бесплатно");
        }else{
            auction.setState(new Sold());
            auction.setStateName("sold");
            if(auction.getPrice()>=1000){
                auction.setHonoraryCode(code.makeCode("Gold-"+auction.getId()));
            }else if(auction.getPrice()>=500 || auction.getPrice()<1000){
                auction.setHonoraryCode(code.makeCode("Silver-"+auction.getId()));
            }else if(auction.getPrice()<500){
                auction.setHonoraryCode(code.makeCode("Bronze-"+auction.getId()));
            }
            System.out.println("Поздравляем победителей");
        }
    }
}
