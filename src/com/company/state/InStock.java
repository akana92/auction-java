package com.company.state;

import com.company.Auction;

public class InStock extends State{
    @Override
    public void startSale(Auction auction) {
        auction.setState(new ForSale());
        auction.setStateName("sale");
        System.out.println("Предмет успешно добавлен на торги");
    }

    @Override
    public void raisePrice(Auction auction) {
        System.out.println("Невозможно изменить цену, товар не в торгах");
    }

    @Override
    public void withdraw(Auction auction) {
        System.out.println("Нельзя снять с торгов товар, который в них не участвует");
    }

    @Override
    public void giveToTheWinner(Auction auction) {
        System.out.println("Невозможно отдать товар сразу со склада");
    }
}
