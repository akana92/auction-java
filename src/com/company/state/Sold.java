package com.company.state;

import com.company.Auction;

public class Sold extends State{
    @Override
    public void startSale(Auction auction) {
        System.out.println("Невозможно начать продажу, так как товар уже продан");
    }

    @Override
    public void raisePrice(Auction auction) {
        System.out.println("Невозможно повысить стоимось, так как товар уже продан");
    }

    @Override
    public void withdraw(Auction auction) {
        System.out.println("Невозможно снять с торгов, так как товар уже продан");
    }

    @Override
    public void giveToTheWinner(Auction auction) {
        System.out.println("Невозможно выдать, так как товар уже выдан");
    }
}
