package com.company.jsonUtils;

import com.company.Auction;
import com.google.gson.Gson;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class JsonWriterAuction {
    private final String path;
    private final Gson gson;

    public JsonWriterAuction(String path, Gson gson, Scanner sc) {
        this.path = path;
        this.gson = gson;
    }

    public void writeGoods(Auction[] auctions) throws IOException {
        FileWriter writer = new FileWriter(path);
        gson.toJson(auctions, writer);
        writer.close();
        System.out.println("Файл успешно обновлен");
    }
}
